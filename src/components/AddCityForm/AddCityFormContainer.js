import { connect } from 'react-redux';
import AddCityForm from './AddCityForm';
import { addCity } from '../../store/actions';

const mapStateToProps = (state, ownProps) => ({
  cityList: state.cities,
  ...ownProps
});

const mapDispatchToProps = {
  addCity
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCityForm);