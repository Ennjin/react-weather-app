import React from 'react';
import { HttpService } from '../../utils/http-service';
import './style.css';

export default class AddCityForm extends React.Component {
  state = {
    loading: false,
    inputCityValue: '',
  };
  
  addCity = async () => {
    const { addCity, cityList } = this.props;
    const { inputCityValue } = this.state;
    const cityInStore = cityList
      .map(e => e.name.toLowerCase())
      .includes(inputCityValue.toLowerCase());
    
    if (!cityInStore) {
      this.setState({ loading: true });
      const params = {
        q: inputCityValue,
      }
      try {
        const { data } = await HttpService.get('/weather/', { params });
        addCity(data);
      } catch (e) {
        alert('Something went wrong. Please try again');
      }
    }
    this.setState({ inputCityValue: '', loading: false });
  }
  
  render() {
    const { loading } = this.state;
    const loader = (
      loading ? <div className="loader"></div> : null
    );
    
    return (
      <div className="row">
        <div className="columns eight">
          <input
            type="text"
            placeholder="Enter City, Tyumen for example"
            value={this.state.inputCityValue}
            onChange={(e) => this.setState({ inputCityValue: e.target.value})}
          />
        </div>
        <div className="columns four">
          <button
            className="button-primary"
            type="button"
            onClick={this.addCity}
            disabled={loading}>
            { loader }
            Add City
          </button>
        </div>
      </div>
    );
  }
}