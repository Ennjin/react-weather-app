import { connect } from 'react-redux';
import WeatherInfo from './WeatherInfo';
import { setCurrentCity } from '../../store/actions';

const mapStateToProps = (state, ownProps) => ({
  city: state.currentCity,
  ...ownProps
});

const mapDispatchToProps = {
  setCurrentCity
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherInfo);
