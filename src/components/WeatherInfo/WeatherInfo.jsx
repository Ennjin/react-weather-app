import React from 'react';
import './style.css';
import { HttpService } from '../../utils/http-service';


export default class WeatherInfo extends React.Component {
  componentDidMount() {
    const { setCurrentCity } = this.props;
    window.navigator.geolocation.getCurrentPosition((position) => {
      const { latitude: lat, longitude: lon } = position.coords;
      HttpService.get('/weather/', { params: { lat, lon }})
        .then(({ data }) => setCurrentCity(data));
    });
  }

  render() {
    const { city } = this.props;
    if (!city) return null;
    return (
      <div className="wrapper">
        <div className="row">
          <div className="one-half column title">
            <span>City</span>
          </div>
          <div className="one-half column">
            <span>{ city.name }</span>
          </div>
        </div>
        <div className="row">
          <div className="one-half column title">
            <span>Temperature</span>
          </div>
          <div className="one-half column">
            <span>{ city.main.temp } C</span>
          </div>
        </div>
        <div className="row">
          <div className="one-half column title">
            <span>Weather</span>
          </div>
          <div className="one-half column">
            <span>{ city.weather[0].description }</span>
          </div>
        </div>
        <div className="row">
          <div className="one-half column title">
            <span>Wind Speed</span>
          </div>
          <div className="one-half column">
            <span>{ city.wind.speed } m/s</span>
          </div>
        </div>
      </div>
    );
  }
}