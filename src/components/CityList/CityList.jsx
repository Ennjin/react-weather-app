import React from 'react';
import AddCityFrom from '../AddCityForm/AddCityFormContainer';
import './style.css';

export default class CityList extends React.Component {

  onDeleteCity = (cityId) => (event) => {
    const { deleteCity } = this.props;
    deleteCity(cityId);
  };

  handleCurrentCity = (city) => (event) => {
    const { setCurrentCity } = this.props;
    setCurrentCity(city);
  };

  render() {
    const renderCityList = this.props.cityList.map((elem, index) => (
      <tr key={index}>
        <td>{ index + 1 }</td>
        <td>{ elem.name }</td>
        <td className="controls">
          <span 
            title="Delete"
            className="pointer"
            onClick={this.onDeleteCity(elem.id)}>  
            <i className="fa fa-2x fa-trash" aria-hidden="true"></i>
          </span>
          <span
            title="Select"
            className="pointer"
            onClick={this.handleCurrentCity(elem)}>
            <i className="fa fa-2x fa-plus" aria-hidden="true"></i>
          </span>
        </td>
      </tr>
    ));

    return (
      <div className="wrapper">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th style={{textAlign: 'center'}}>Controls</th>
            </tr>
          </thead>
          <tbody>
            { renderCityList }
          </tbody>
        </table>
        <AddCityFrom />
      </div>
    )
  }
}
