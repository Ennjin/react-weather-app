import { connect } from 'react-redux';
import CityListComponent from './CityList';
import { deleteCity, setCurrentCity } from '../../store/actions';

const mapStateToProps = (state, ownProps) => ({
  cityList: state.cities,
  ...ownProps,
});

const mapDispatchToProps = {
  deleteCity,
  setCurrentCity
};

export default connect(mapStateToProps, mapDispatchToProps)(CityListComponent);