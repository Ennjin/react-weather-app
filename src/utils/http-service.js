import Axios from 'axios';

export const HttpService = Axios.create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
  params: {
    // В реальном приложении все секретные данные хранились бы на сервере
    // или в виртуальном окружении
    appid: 'OPENWEATHERMAP API KEY',
    units: 'metric'
  }
});