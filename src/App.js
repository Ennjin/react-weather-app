import React, { Component } from 'react';
import CityList from './components/CityList/CityListContainer';
import WeatherInfo from './components/WeatherInfo/WeatherInfoContainer';

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <h4>City List</h4>
            <CityList />
          </div>
          <div className="one-half column">
            <h4>Weather</h4>
            <WeatherInfo />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
