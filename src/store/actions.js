export const ADD_CITY = '[Cities] Add city';
export const DELETE_CITY = '[Cities] Delete city';
export const SET_CURRENT_CITY = '[Cities] Set current city';

export function addCity(city) {
  return {
    type: ADD_CITY,
    payload: city
  };
}

export function deleteCity(cityId) {
  return {
    type: DELETE_CITY,
    payload: cityId
  };
}

export function setCurrentCity(city) {
  return {
    type: SET_CURRENT_CITY,
    payload: city
  };
}