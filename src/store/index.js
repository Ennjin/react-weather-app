import { createStore } from 'redux';
import { weatherApp } from './reducers';

export const weatherStore = createStore(weatherApp);