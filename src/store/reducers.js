import { 
  ADD_CITY, 
  DELETE_CITY, 
  SET_CURRENT_CITY
} from './actions';

const initialState = {
  cities: [],
  currentCity: null
};

export function weatherApp(state = initialState, action) {
  switch (action.type) {
    case ADD_CITY:
      return {
        ...state,
        cities: [...state.cities, action.payload] 
      };
    case DELETE_CITY:
      return {
        ...state,
        cities: state.cities.filter(elem => elem.id !== action.payload)
      };
    case SET_CURRENT_CITY:
      return {
        ...state,
        currentCity: action.payload
      };
    default:
      return state;
  }
}