import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/skeleton.css';
import './assets/css/font-awesome.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { weatherStore } from './store';

const appWithStore = (
  <Provider store={weatherStore}>
    <App />
  </Provider>
);

ReactDOM.render(appWithStore, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
